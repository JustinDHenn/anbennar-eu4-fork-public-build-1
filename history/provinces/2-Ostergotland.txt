#Östergötland, contains Norrköping, Linköping etc.

owner = A30
controller = A30
add_core = Z42
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 6
base_production = 5
base_manpower = 5

trade_goods = cloth

capital = ""

is_city = yes
fort_15th = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
discovered_by = tech_gnollish
