#369 - Orkney

owner = A47
controller = A47
add_core = A47
culture = tefori
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 2

trade_goods = copper

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}

add_permanent_province_modifier = {
	name = halfling_minority_coexisting_large
	duration = -1
}