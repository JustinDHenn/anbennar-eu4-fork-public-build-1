#526 - Tungr

owner = F23
controller = F23
add_core = F23
culture = copper_dwarf
religion = ancestor_worship

base_tax = 12
base_production = 14
base_manpower = 8

trade_goods = copper
center_of_trade = 2

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari