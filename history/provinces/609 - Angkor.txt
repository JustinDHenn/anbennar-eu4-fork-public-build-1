# No previous file for Angkor
owner = F42
controller = F42
add_core = F42
culture = sun_elf
religion = bulwari_sun_cult

hre = no

base_tax = 4
base_production = 3
base_manpower = 2

trade_goods = incense

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = human_minority_coexisting_large
	duration = -1
}
