# No previous file for Leslinp�r
owner = A92
controller = A92
add_core = A92
culture = esmari
religion = regent_court

hre = yes

base_tax = 8
base_production = 7
base_manpower = 5

trade_goods = grain
center_of_trade = 1

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}