# No previous file for Tsaidam
owner = Z33
controller = Z33
add_core = Z33
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 3

trade_goods = fur

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold