# No previous file for Bechbaliq
owner = Z23
controller = Z23
add_core = Z23
culture = white_reachman
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish