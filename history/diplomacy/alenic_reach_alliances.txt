vassal = {
	first = Z22
	second = Z21
	start_date = 1420.1.1
	end_date = 2019.9.25
}
alliance = {
	first = Z22
	second = Z24
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
alliance = {
	first = Z22
	second = Z29
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
alliance = {
	first = Z22
	second = Z28
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
alliance = {
	first = Z22
	second = Z25
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
alliance = {
	first = Z22
	second = Z30
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
alliance = {
	first = Z22
	second = Z32
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
alliance = {
	first = Z22
	second = Z33
	trade_league = 1
	start_date = 1380.1.1
	end_date = 1581.1.1
}
guarantee = {
	first = Z22
	second = Z31
	start_date = 1399.1.1
	end_date = 1909.1.1
}