##################################################################
### Terrain Categories
###
### Terrain types: plains, mountains, hills, desert, artic, forest, jungle, marsh, pti
### Types are used by the game to apply certain bonuses/maluses on movement/combat etc.
###
### Sound types: plains, forest, desert, sea, jungle, mountains

categories =  {
	pti = {
		type = pti
	}

	ocean = {
		color = { 255 255 255 }

		sound_type = sea
		is_water = yes

		movement_cost = 1.0
	}

	inland_ocean = {
		color = { 0 0 200 }

		sound_type = sea
		is_water = yes
		inland_sea = yes

		movement_cost = 1.0
	}	

	glacier = {
		color = { 235 235 235 }

		sound_type = desert

		defence = 1
		movement_cost = 1.25
		supply_limit = 2		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75		
		
		terrain_override = {		
			2798 1770 2800 2801 1804 2802 2516 1230 1234 1228 1227 2787
			#1246 1248 1245 1244 1231 1229 1222 1232 1233 1220 1225 1219 1223 1226
		}
	}

	farmlands = {
		color = { 179 255 64 }

		type = plains
		sound_type = plains

		movement_cost = 1.10
		local_development_cost = -0.05
		supply_limit = 10
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1.05
		
		terrain_override = {
			88 84 81 97 80 79 95 114 73 38 74 70 78 82 83 119 87 125 128 133 132 131 130 136 137 135 152 166 162 153 156 10 15 13 34 49 43 46 27 23 24 86 20 113
			143 103 147 165 149 12 207 347 177 176 175 181 171 158 44 159 142
			251 233 908 253 236 220 337 339 335 256 914 262 168 3 6 408 163
			
			85 108
			#Dameria
			39 50 281 285 284 910 330 275 297 280 916 274 333 269 267 917 109 286 293 298
			428 432 433 #Corvuria
			
			#Lencenor
			32 59 124
			
			#Dragon Coast
			172 144 145
			
			#Borders
			325 904 921 412 415 919 319 315 300 332 923 579 907
			
			#Esmaria
			311 310 265 901 909 902 279
			
			#Alenic Expanse
			218 221 340 243 219 245
			
			#Alenic Reach
			695 699 724 723 721 709 718
			
			#Escann
			779 777 774 781 756 757 765 792 835 834 837 839 829 830 821 825 827 826 856 876 885 818 817 816 810 819 800 798 797
			
			#Bulwar
			574 567 584 560 586 596 598 600 599 602 604
			607 612 625 628 631 634 635 639 640 641 642 643 608 613 615 616
			622 621 496 516 521 538 543
			536
			
			#Kheterata
			468 469 471 472 473 474 475
			
			#Aelantir
			2745 2773 2738 2736 2728 2785 2765 2761 2680 2708 2674 2669 2595 2598 2625 2621 2702 2651 2650 2619 2677 2542
			2484 2490 2508 2520 2560 2565 2469 2465 2543 2500 2495 2497 2496 2493 2501 2558 2556 2545 2541 2483
			2301 2296
			2557 1016 1999 1111 1015 1110 1037 1047 1939 1030 1035
			1202 1207 1203 1273 1931 2854
			
			#Ynn
			1833 1133 1134 1136 1827 1142 2859 1148 2810 1138 1825 1139 1141 1820 1166 2839 2862 2809 1168 1169 1171 1180 2840 2814 1189 1816 1191 1758 1193 2843
			1155 1158 1151 1161 2807 2819 1162 
			
			#Eordand
			2037 2041 2028 2022 2031 2008 2026 2007 2036 2024 2050 2014 1875 1969 1974 1965 1963
		
		}		
	}
	
	forest = {
		color = { 31 116 16 }

		type = forest
		sound_type = forest
		
		movement_cost = 1.25
		defence = 1
		supply_limit = 4
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		
		terrain_override = { 
			110 57 5 228 178 140 137 292 717

		}                                      
	}
	
	hills = {
		color = { 113 176 151 }

		type = hills
		sound_type = mountains

		movement_cost = 1.40
		defence = 1
		local_defensiveness = 0.1
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		supply_limit = 5
		
		terrain_override = {
			100 93 90 72 71 161 160 18 28 17 167 148 150 211 212 336 217 346 254 367 362 2 170 174 173 48 366 838 306 305 354
			37 276 277 261 304 303 91
			424  #Corvuria
			
			#Bulwar
			675 678 677 523 514 515 528 530 531
			
			#Aelantir 
			1094 1074 2091 2318 2460 
			2780 2782 2778 2777 2776 2770 2768
			2704 2647 2660 2661 2629 2631 2630 2618 2663 2636 2640 2697 2701 2711 2705 2676 2639
			2423 2486 2473 2474 2477 2475 2478 2481 2552 2551 2547 2542 2530 2546 2534 2525 2571 2572 2502 2491
			2374 2362 2177 2187 2197 
			2488 1144 1153 2844 1159 1176 1201 2850 1211 1773 1989 1769 1984 1772 1776
			1993 1125 1997 2010 2052 2054 2055 1209
			1178
		}
	}

	
	woods = {
		color = { 41 155 22 }

		type = forest
		sound_type = forest

		movement_cost = 1.10
		defence = 1
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.9
		supply_limit = 6
		
		terrain_override = {
			94 77 120 102 69 61 134 139 362 29 282 278 42 40 323 322 222 223 
			
			#East Dameshead
			394 289
			
			#Borders
			418 321 414 918 318
			
			#Esmaria
			264 917 269 273 272 900 309 259
			
			#Escann
			795 853 814
			
			#Alenic Reach
			722 712 713 714 
			
			#Aelantir
			2539 2562 2570
			1154 2742 925 1007 1005 1006 932 1073

		}
	}
	
	mountain = {
		color = { 105 24 4 }

		type = mountains
		sound_type = mountains

		movement_cost = 1.5
		defence = 2
		local_defensiveness = 0.25
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.75
		supply_limit = 4
		
		terrain_override = {
			64 65 350 356 270 4 
			441 438 436 
			
			#Aelantir
			2779 2786 
			2649 2648 2658 2628 2627 2666 2667 2646 2637 2638 2715 2714 2659 2657
			2400 2454 2507 2512
			2381 2379 2380 2360 2361 
			
			2838
		}
	}

	impassable_mountains = {
		color = { 128 128 128 }

		sound_type = desert

		movement_cost = 8.0
		defence = 6
		local_development_cost = 10		
	}

	grasslands = {
		color = { 130 255 47 }

		type = plains
		sound_type = plains

		movement_cost = 1.0
		supply_limit = 8
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1
		
		terrain_override = {
			89 
		}
	}

	jungle = {
		color = { 98 163 18 }

		type = jungle
		sound_type = jungle

		movement_cost = 1.5
		defence = 1
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		supply_limit = 5
		
		terrain_override = {
			1057 1093 1092
			
			#Aelantir
			2554 2499
		}
	}	
	
	marsh = {
		color = { 13 189 130 }

		type = marsh
		sound_type = forest

		movement_cost = 1.3
		defence = 1
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		supply_limit = 5

		terrain_override = {
			#North Salahad
			463 466 503 464 462 467 546
		
			#Bulwar
			564 572 561 562 563
			
			#Aelantir 
			1873 1901 1033 1149 1165
		
		}
	}
	
	desert = {
		color = { 242 242 111 }

		type = desert
		sound_type = desert

		movement_cost = 1.05
		supply_limit = 4
		local_development_cost = 0.50
		nation_designer_cost_multiplier = 0.8
		
		terrain_override = { 
			#Aelantir
			2550
		}
	}
	
	coastal_desert = {
		color = { 255 211 110 }

		type = desert
		sound_type = desert

		movement_cost = 1.0
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.9		
		supply_limit = 4

		terrain_override = {
			2124 2911 2912 2913 2923
		}	
	}
	
	coastline = {
		color = { 49 175 191 }

		sound_type = sea

		movement_cost = 1.0
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85		
		supply_limit = 6 
		
		terrain_override = {
			9 14 146 169 51 406 452 1086 1071 931 930 929 1079 1078 1077 1059 953 
			
			#Aelantir
			2739 2719 2718 2717 
			2561 2329 2326
			1952 2061 2157 2170 1681 1105 926 927 925 1001 2796 2794 2793 2790 1987
		}
	}	
	
	drylands = {
		color = { 232 172 102 }		

		type = plains
		sound_type = plains

		movement_cost = 1.00
		local_development_cost = 0.05
		nation_designer_cost_multiplier = 0.95
		supply_limit = 7
		allowed_num_of_buildings = 1

		terrain_override = {
			386 385 387 389 384 400 403 382 404
			
			#Bulwar
			534 537 539 540
			570 568 559 595 594 593 589 590 611 628 624 629 630 633 636 637 614 620 623
			
			#Aelantir
			2762 2759 2600 2599 2598 2597 2596 2601 2602 2603
			2453 2451 2442 2441 2440 2432 2439 2447 2388 2416 2385 1195 2456 2458 2457
			2580 2574 2575 2576 2566 2559 2510 2509 2515 2536 2517 2518 2519 2549 2505 2504 2540 2555 2514 2529
			2297 2303 2304 2305 2302 2307 2308
		}		
	}

	highlands = {
		color = { 176 129 21 }

		type = hills
		sound_type = mountains
		
		supply_limit = 6
		movement_cost = 1.40
		defence = 1
		local_defensiveness = 0.1
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9	

		terrain_override = {
			179 180 182 184 196 197 200 371 370 317 316 375 376 291 290 374 379 35 369 380 36 399 381 405 393 391
			409 913
			
			#Escann
			
			
			#Bulwar
			529 520 522 518 580
			617 618 577 573 679 597 693 692 686 691
			551 552 555 554 658	545 654 663 664 665 619 671 670 672 673 674
			
			#Aelantir
			2754 2766 2760 2755 2758 2756 2763 2764 2767 2733 2737 2772 2730 2732
			2709 2713 2673 2710 2700 2699 2678 2638 2624
			2386 2387 2382 2455 
			2468 2470 2472 2487 2568 2527 2494 2464 2466 2529 2511 2506 2553
			2345 2346 2347 2337 2250 2252 2295
			2056 2057 2060
		}
	}

	savannah = {
		color = { 248 199 23  }

		sound_type = plains

		supply_limit = 6
		movement_cost = 1.00
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.95	
		
		terrain_override = {
			2093 1056 1221 1084
			
			#Aelantir 
			2092 2133 2130 2129 2127 2824 2822 2823 2097 2096 2827 2059 
			
			2685 2410 1992 1019 1834 1835 
		}		
	}
	
	steppe = {
		color = { 147 200 83  }

		type = plains
		sound_type = plains

		movement_cost = 1.00
		local_development_cost = 0.20
		nation_designer_cost_multiplier = 0.9	
		supply_limit = 6
		
		terrain_override = {
			#Aelantir
			1792 1847 1848 2842 1851 2729 1858 1859 1843 1844 1862 1842 1845 1881 1857 1855 1853
		}	
	}
	
	
	dwarven_hold = {
		color = { 255 198 0 }

		type = mountains
		sound_type = mountainss

		defence = 2
		movement_cost = 1.3
		supply_limit = 12		
		local_development_cost = -0.05
		local_defensiveness = 0.25
		allowed_num_of_buildings = 2
		nation_designer_cost_multiplier = 1.2		
		
		terrain_override = {		
			62 526 912 2914
		}
	}


	dwarven_road = {
		color = { 36 42 75 }

		type = mountains
		sound_type = mountainss

		defence = 2
		movement_cost = 0.90
		supply_limit = 6		
		local_development_cost = 0.10
		local_defensiveness = 0.20
		nation_designer_cost_multiplier = 0.95		
		
		terrain_override = {		
			263 
		}
	}

	cavern = {
		color = { 0 0 0 }

		type = mountains
		sound_type = mountainss

		defence = 2
		movement_cost = 1.50
		supply_limit = 5	
		local_development_cost = 0.8
		local_defensiveness = 0.25
		nation_designer_cost_multiplier = 0.85		
		
		terrain_override = {				
			185 186 187 189
		}
	}

	city_terrain = {
		color = { 225 10 30 }

		type = forest
		sound_type = plains

		defence = 1
		movement_cost = 1.25
		supply_limit = 12	
		local_development_cost = -0.10
		local_defensiveness = 0.1
		allowed_num_of_buildings = 2
		nation_designer_cost_multiplier = 1.2		
		
		terrain_override = {				
			8 #Anbenncost
			52 #Port Munas
			63 66 #Viswall
			67 #Lorentaine
			101 #Wineport
			126 #Portnamm
			151 #Beepeck
			216 #Vertesk
			266 #Esmaraine
			328 #Silvelar
			327 #Cestaire
			234 #Damescrown
			294 #Magdalaire
			451 #Nathalaire
			565 #Brasan
			569 #Menibor
			587 #Bellacaire
			831 832 833 #Castonath
			141 165
			33 26 15 329 331 365 911 249 915 112 601 271 164 417
			
		}
	}
}
	
##################################################################
### Graphical terrain
###		type	=	refers to the terrain defined above, "terrain category"'s 
### 	color 	= 	index in bitmap color table (see terrain.bmp)
###

terrain = {
	grasslands			= { type = grasslands		color = { 	0	 } }
	hills				= { type = hills			color = { 	1	 } }
	desert_mountain		= { type = mountain			color = { 	2	 } }
	desert				= { type = desert			color = { 	3	 } }

	plains				= { type = grasslands		color = { 	4	 } }
	terrain_5			= { type = grasslands		color = { 	5	 } }
	mountain			= { type = mountain			color = { 	6	 } }
	desert_mountain_low	= { type = desert			color = { 	7	 } }

	terrain_8			= { type = hills			color = { 	8	 } }
	marsh				= { type = marsh			color = { 	9	 } }
	terrain_10			= { type = farmlands		color = { 	10	 } }
	terrain_11			= { type = farmlands		color = { 	11	 } }

	forest_12			= { type = forest			color = { 	12	 } }
	forest_13			= { type = forest			color = { 	13	 } }
	forest_14			= { type = forest			color = { 	14	 } }
	ocean				= { type = ocean			color = { 	15	 } }

	snow				= { type = mountain 		color = { 	16	 } } # (SPECIAL CASE) Used to identify permanent snow
	inland_ocean_17 	= { type = inland_ocean		color = {	17	 } }

	coastal_desert_18	= { type = coastal_desert	color = { 	19	 } }
	coastline			= { type = coastline		color = { 	35	 } }
	
	savannah			= { type = savannah 		color = {	20	 } }
	drylands			= { type = drylands			color = {	22	 } }
	highlands			= { type = highlands		color = {	23	 } }
	dry_highlands		= { type = highlands		color = {	24	 } }
	
	woods				= { type = woods			color = { 	255	 } }
	jungle				= { type = jungle			color = { 	254	 } }
	
	terrain_21			= { type = farmlands		color = { 	21	 } }	
}

##################################################################
### Tree terrain
###		terrain	=	refers to the terrain tag defined above
### 	color 	= 	index in bitmap color table (see tree.bmp)
###

tree = {
	forest				= { terrain = forest 			color = { 	3 4 6 7 19 20	} }
	woods				= { terrain = woods 			color = { 	2 5 8 18	} }
	jungle				= { terrain = jungle 			color = { 	13 14 15	} }
	palms				= { terrain = desert 			color = { 	12	} }
	savana				= { terrain = grasslands 		color = { 	27 28 29 30	} }
}